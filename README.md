# Transaction Reporting System

Run instructions: 

1. keep source file in src/main/resources directory

2. update the below 4 properties accordingly in main method.

    private static final String sourceFileName = "src/main/resources/transactions.csv";
    
    private static final String merchant = "Kwik-E-Mart";
    
    private static final String start = "20/08/2018 12:00:00";
    
    private static final String end = "20/08/2018 13:00:00";
    
	
3. run main program without any parameters.


Details:


There is a DataLoading interface, and CSVDataLoading implementation which is used to read csv and populate DataStore class

DataStore class holds the current transaction by merchants, and list of reversals

ReportAverageTransaction class have only two member variables to hold average amount and count.

3 test cases are written 