package com.hoolah.test;

import com.hoolah.com.hoolah.service.CSVDataLoadingService;
import com.hoolah.com.hoolah.service.DataLoadingService;
import com.hoolah.com.hoolah.service.TransactionService;
import com.hoolah.data.DataStore;
import com.hoolah.model.ReportAverageTransaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

public class TransactionServiceTest {

    private static TransactionService transactionService;

    @BeforeClass
    public static void setUp() throws IOException {
        DataLoadingService service = new CSVDataLoadingService();
        DataStore dataStore = service.load("src/test/resources/transactions.csv");
        transactionService = new TransactionService(dataStore);
    }

    @Test
    public void test_ReverseTransaction() {
        ReportAverageTransaction report = transactionService.generateReportByMerchantName("Kwik-E-Mart", TransactionService.convert("20/08/2018 12:00:00"), TransactionService.convert("20/08/2018 13:00:00"));
        Assert.assertEquals(59.99, report.getAmount(), 0);
        Assert.assertEquals(1, report.getCount());
    }

    @Test
    public void test_no_transaction() {
        ReportAverageTransaction report = transactionService.generateReportByMerchantName("Kwik-E-Mart2", TransactionService.convert("20/08/2018 12:00:00"), TransactionService.convert("20/08/2018 13:00:00"));
        Assert.assertEquals(0d, report.getAmount(), 0);
        Assert.assertEquals(0, report.getCount());
    }

    @Test
    public void test_outrange() {
        ReportAverageTransaction report = transactionService.generateReportByMerchantName("Kwik-E-Mart", TransactionService.convert("20/08/2019 12:00:00"), TransactionService.convert("20/08/2019 13:00:00") );
        Assert.assertEquals(0d, report.getAmount(), 0);
        Assert.assertEquals(0, report.getCount());
    }

}

