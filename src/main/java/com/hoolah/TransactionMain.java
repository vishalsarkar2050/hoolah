package com.hoolah;

import com.hoolah.com.hoolah.service.CSVDataLoadingService;
import com.hoolah.com.hoolah.service.DataLoadingService;
import com.hoolah.com.hoolah.service.TransactionService;
import com.hoolah.data.DataStore;
import com.hoolah.model.ReportAverageTransaction;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class TransactionMain {

    private static final Logger log = LoggerFactory.getLogger(TransactionMain.class);

    private static final String sourceFileName = "src/main/resources/transactions.csv";
    private static final String merchant = "Kwik-E-Mart";
    private static final String start = "20/08/2018 12:00:00";
    private static final String end = "20/08/2018 13:00:00";

    public static void main(String[] args) throws IOException {

        System.out.println("Starting Transaction Main");
        TransactionService transactionService;
        DataLoadingService service = new CSVDataLoadingService();
        DataStore dataStore = service.load(sourceFileName);
        transactionService = new TransactionService(dataStore);

        ReportAverageTransaction reportAverageTransaction =
                transactionService.generateReportByMerchantName(
                        merchant,
                        TransactionService.convert(start),
                        TransactionService.convert(end));

        System.out.println("Report = "+ reportAverageTransaction);
        System.out.println("Transaction Main finished");
    }
}
