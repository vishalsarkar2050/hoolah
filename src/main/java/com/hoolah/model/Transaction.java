package com.hoolah.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Data
public class Transaction {

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    private String id;
    private String merchant;
    private LocalDateTime transactionDate;
    private TransactionType transactionType;
    private String relatedTransactionId;
    private double amount;
    private long transactionDateEpoch;
    private boolean reversed;

    public Transaction(String[] csvRow){
        this.id = csvRow[0].trim();
        this.transactionDate = LocalDateTime.parse(csvRow[1].trim(), formatter);
        this.amount = Double.parseDouble(csvRow[2].trim());
        this.merchant = csvRow[3].trim();
        this.transactionType = TransactionType.valueOf(csvRow[4].trim());
        this.relatedTransactionId = csvRow.length > 5? csvRow[5].trim(): null;
        this.transactionDateEpoch = this.transactionDate.toEpochSecond(ZoneOffset.UTC);
    }

}
