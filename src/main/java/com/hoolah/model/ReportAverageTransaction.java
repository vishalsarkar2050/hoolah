package com.hoolah.model;

import lombok.Data;

@Data
public class ReportAverageTransaction {

    private double amount;
    private int count;
    private double average;

    @Override
    public String toString() {
        return "ReportAverageTransaction{" +
                "count=" + count +
                ", average=" + average +
                '}';
    }

    public ReportAverageTransaction(double amount, int count) {
        this.amount = amount;
        this.count = count;
    }

    public void addAmount(double amount){
        this.amount += amount;
    }
    public void incrementCount(){
        count++;
    }
    public void adjustAverage(){
        if(count != 0){
            average = amount / count;
        }
    }

}
