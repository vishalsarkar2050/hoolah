package com.hoolah.com.hoolah.service;

import com.hoolah.data.DataStore;
import com.hoolah.model.ReportAverageTransaction;
import com.hoolah.model.Transaction;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class TransactionService {

    private DataStore dataStore;

    public TransactionService(DataStore layer){
        this.dataStore = layer;
    }

    public static LocalDateTime convert(String date){
        return LocalDateTime.parse(date, Transaction.formatter);
    }

    public ReportAverageTransaction generateReportByMerchantName(String merchantName, LocalDateTime start, LocalDateTime end) {
        final ReportAverageTransaction reportAverageTransaction = new ReportAverageTransaction(0,0);
        long startEpoch = start.toEpochSecond(ZoneOffset.UTC);
        long endEpoch = end.toEpochSecond(ZoneOffset.UTC);

        List<Transaction> txns = this.dataStore.getTransactionsMap().get(merchantName);

        if(txns == null ){
            return reportAverageTransaction;
        }

       txns.stream()
                .filter(transaction -> {
                    return ! dataStore.getReversals().contains(transaction.getId())
                            && transaction.getTransactionDate().toEpochSecond(ZoneOffset.UTC) >= startEpoch
                            && transaction.getTransactionDate().toEpochSecond(ZoneOffset.UTC) <= endEpoch; })
                .forEach(txn->{
                        reportAverageTransaction.incrementCount();
                        reportAverageTransaction.addAmount(txn.getAmount());
                });
        reportAverageTransaction.adjustAverage();
        return reportAverageTransaction;
    }
}
