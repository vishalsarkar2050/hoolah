package com.hoolah.com.hoolah.service;

import com.hoolah.data.DataStore;

import java.io.IOException;

public interface DataLoadingService {

    DataStore load(String uri) throws IOException;

}
