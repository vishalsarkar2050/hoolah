package com.hoolah.com.hoolah.service;

import com.hoolah.data.DataStore;
import com.hoolah.model.Transaction;
import com.hoolah.model.TransactionType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class CSVDataLoadingService implements DataLoadingService {

    DataStore dataStore ;
    private final String SEPERATOR = ",";

    public CSVDataLoadingService(){
        this.dataStore = new DataStore();
    }

    @Override
    public DataStore load(String uri) throws IOException {

       Map<String, List<Transaction>> transactions =  Files.lines(Paths.get(uri))
                .skip(1)
                .map(csv->new Transaction(csv.split(SEPERATOR)))
                .peek(transaction -> {
                    if(transaction.getTransactionType() == TransactionType.REVERSAL){
                        dataStore.getReversals().add(transaction.getRelatedTransactionId());
                    }
                })
                .collect(Collectors.groupingBy(Transaction::getMerchant));
        dataStore.setTransactionsMap(transactions);
        return dataStore;
    }
}
