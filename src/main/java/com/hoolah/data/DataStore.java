package com.hoolah.data;

import com.hoolah.model.Transaction;
import lombok.Data;

import java.util.*;

@Data
public class DataStore {

    private boolean dataLoaded;
    private Map<String, List<Transaction>> transactionsMap;
    private HashSet<String> reversals;

    public DataStore(){
        this.reversals = new HashSet<>();
    }
}
